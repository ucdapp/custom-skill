import Base from '../Base/Controller';

class Error extends Base {
  async run(): Promise<any> {
    this.outputSpeechText('no match request');
  }

}

export default Error;
