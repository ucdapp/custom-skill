import Base from '../Base/Controller';

class SessionEnded extends Base {
  async run(): Promise<any> {
    this.skill.getResponse().shouldEndSession = true;
  }

}

export default SessionEnded;
