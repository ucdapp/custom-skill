import OutputSpeechType from '@twbdrd/alexa-skill-type/source/Custom/Enumeration/OutputSpeechType';
import Skill from '../Skill';
import Base from './Base';

export type ControllerConstructor = { new(skill: Skill): Controller };

abstract class Controller extends Base {
  readonly skill: Skill;

  // noinspection TypeScriptAbstractClassConstructorCanBeMadeProtected
  constructor(skill: Skill) {
    super();
    this.skill = skill;
  }

  async run(): Promise<any> {
  }

  protected outputSpeechMarkup(ssml: string, shouldEndSession: boolean = true) {
    const response = this.skill.response.response;
    response.outputSpeech = {
      type: OutputSpeechType.SSML,
      text: ssml
    };
    response.shouldEndSession = shouldEndSession;
  }

  protected outputSpeechText(text: string, shouldEndSession: boolean = true) {
    const response = this.skill.response.response;
    response.outputSpeech = {
      type: OutputSpeechType.PlainText,
      text
    };
    response.shouldEndSession = shouldEndSession;
  }

}

export default Controller;
