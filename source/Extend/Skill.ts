import RequestType from '@twbdrd/alexa-skill-type/source/Custom/Enumeration/RequestType';
import {Context, IntentRequest, Request, RequestBody, ResponseBody} from 'alexa-sdk';
import {InspectOptions} from 'util';
import Base from './Base/Base';
import Controller, {ControllerConstructor} from './Base/Controller';
import {IntentConstructor} from './Controller/Intent';
import IntentError from './Intent/Error';
import RequestError from './Request/Error';
import Launch from './Request/Launch';
import SessionEnded from './Request/SessionEnded';

export default abstract class Skill extends Base {
  readonly callback: Function;
  readonly context: Context;
  readonly event: RequestBody<Request>;
  readonly response: ResponseBody = {
    version: '1.0',
    sessionAttributes: {},
    response: {shouldEndSession: true}
  };

  // noinspection TypeScriptAbstractClassConstructorCanBeMadeProtected
  constructor(event: RequestBody<Request>, context: Context, callback: Function) {
    super();
    this.event = event;
    this.context = context;
    this.callback = callback;
    this.response.sessionAttributes = this.getSessionAttributes();
  }

  getIntent(): Controller {
    const request: IntentRequest = this.event.request;
    const intent = request.intent;

    if (undefined === intent) {
      throw this.response;
    }

    const name = intent.name;
    const items = this.makeIntents();
    const classConstructor = items.hasOwnProperty(name) ? items[name] : IntentError;
    return new classConstructor(this);
  };

  abstract getProvider(): Controller;

  getRequest(): Controller {
    const type = this.event.request.type;
    const items = this.makeRequests();
    const classConstructor = items.hasOwnProperty(type) ? items[type] : RequestError;
    return new classConstructor(this);
  };

  getResponse() {
    return this.response.response;
  }

  getSession() {
    return this.response.sessionAttributes;
  }

  getSessionAttributes() {
    const items = this.event.session.attributes;
    return (undefined === items) ? {} : items;
  }

  async run() {
    try {
      const options: InspectOptions = {depth: null};
      this.debug('Request', this.event, options);
      await this.getController().run();
      this.debug('Response', this.response, options);
      this.callback(null, this.response);
    } catch (error) {
      this.errorHandler(error);
    }
  }

  protected errorHandler(error: any) {
    if (error instanceof Error) {
      this.debug('Error', error.message);
      this.callback(null, this.response);
    } else {
      this.debug('Error', error);
      this.callback(error);
    }
  }

  protected getController(): Controller {
    return (RequestType.IntentRequest === this.event.request.type) ? this.getIntent() : this.getRequest();
  }

  // noinspection JSMethodCanBeStatic
  protected makeIntents(): { [index: string]: IntentConstructor } {
    return {};
  }

  // noinspection JSMethodCanBeStatic
  protected makeRequests() {
    const items: { [index: string]: ControllerConstructor } = {};
    items[RequestType.LaunchRequest] = Launch;
    items[RequestType.SessionEndedRequest] = SessionEnded;
    return items;
  }

}
