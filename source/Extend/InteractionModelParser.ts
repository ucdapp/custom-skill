import InteractionModel from '@twbdrd/alexa-skill-type/source/Custom/InteractionModel';
import Type from '@twbdrd/alexa-skill-type/source/Custom/InteractionModel/LanguageModel/Type';
import Value from '@twbdrd/alexa-skill-type/source/Custom/InteractionModel/LanguageModel/Value';
import {readFileSync} from 'jsonfile';

export type SynonymMap = Map<string, Map<string, string>>;

class InteractionModelParser {
  protected model?: InteractionModel;
  protected readonly path: string;
  protected synonymMap: SynonymMap = new Map<string, Map<string, string>>();

  constructor(path: string) {
    this.path = path;
  }

  getModel(): InteractionModel {
    const item = (this.model) ? this.model : readFileSync(this.path);
    this.model = item;
    return item;
  }

  // noinspection FunctionWithMultipleReturnPointsJS
  getSynonymMap(): SynonymMap {
    const map = this.synonymMap;

    if (0 < map.size) {
      return map;
    }

    this.getTypes().forEach(item => {
      map.set(item.name, this.makeSynonymItems(item.values));
    });

    return map;
  }

  protected getTypes(): Type[] {
    const types = this.getModel().interactionModel.languageModel.types;
    return types ? types : [];
  }

  protected makeSynonymItems(items: Value[]) {
    const map = new Map<string, string>();

    items.forEach(item => {
      const {value, synonyms} = item.name;
      const data = ((undefined === synonyms) || (0 === synonyms.length)) ? value : synonyms[0];
      map.set(value, data);
    });

    return map;
  }

}

export default InteractionModelParser;
